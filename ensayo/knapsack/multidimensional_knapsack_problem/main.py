import sys, json, argparse
from copy import deepcopy
from knapsack import *

parser = argparse.ArgumentParser()
parser.add_argument('--attr', type=str)
parser.add_argument('--gold', type=int)
parser.add_argument('--file', type=int)
parser.add_argument('--size', type=int)
args = parser.parse_args()
attr_list = args.attr.split(',') # ['1','2','3','4']

VECTOR =  { # Pesos de la ecuacion lineal de fitness
        "Agility":int(attr_list[0]),
        "Mana":int(attr_list[1]),
        "Health":int(attr_list[2]),
        "Mana Regeneration":int(attr_list[3]),
        "HP Regeneration":int(attr_list[4]),
        "Strength":int(attr_list[5]),
        "Damage":int(attr_list[6]),
        "Intelligence":int(attr_list[7]),
        "Spell Resistance":int(attr_list[8]),
        "Armor":int(attr_list[9]),
        "Movement Speed":int(attr_list[10]),
        "Attack Speed":int(attr_list[11])
        }

def items_from_json(filename):
    items_json = []
    items = []
    with open(filename) as data:
        items_json = json.load(data)

    items_json = items_json[:args.file]
    for i in items_json:
        coste = 0  if i["Cost"] is None else int(i["Cost"])
        item = Item(i["Id"], i["ItemName"], coste, i["Attributes"], knapsack.calculateFitness(i["Attributes"], VECTOR), VECTOR)
        if i["Components"] is None:
            item.addComponents([])
        elif len(i["Components"]) > 0:
            item.addComponents(i["Components"])
        items.append(item)
    return items

def bag_from_file(filename):
    return (args.size, args.gold, items_from_json("items.json"))

if __name__ == '__main__':
    # items = [
    #             Item(name, value, weight, volume) for name, value, weight, volume in \
    #             [
    #                 ('Apple', 2, 1, 2), ('Laptop', 3, 2, 1), ('Pen', 3, 1, 3),
    #                 ('Cap', 4, 3, 2), ('Cookie', 4, 2, 2), ('iPod', 5, 3, 3)
    #             ]
    #         ]

    bag = Knapsack(*bag_from_file('items.json'), tabu_list=TabuList(200))
    #random_add_solution comes from initial_solution.py
    #all_neighborhood comes from neightborhood.py
    bag.optimize(random_add_solution, TabuSearch(300), all_neighborhood)
    # bag.optimize(random_add_solution, first_improving_neighborhood, first_improving)
