from time import clock
from copy import deepcopy

num_attr = 0

def calculateFitness(attributeList, vector):
    # Creamos el mapa de atributos
    atributacos = {
    "Agility":0,
    "Mana":0,
    "Health":0,
    "Mana Regeneration":0,
    "HP Regeneration":0,
    "Strength":0,
    "Damage":0,
    "Intelligence":0,
    "Spell Resistance":0,
    "Armor":0,
    "Movement Speed":0,
    "Attack Speed":0,
    "All Attributes":0
    }

    global num_attr
    if num_attr == 0: #Calcula el numero de atributos a evaluar
        for atributo in vector:
            if vector[atributo] != 0:
                num_attr += 1
    
    if not bool(attributeList): # Evita listas vacias
        return 0
    for atributo in attributeList:
        try:
            atributacos[atributo["Name"]] += int(atributo["Value"][1:])
        except KeyError: # Evita atributos que no estamos contemplando
            #print 'no existe la entrada en ' + atributo["Name"]
            pass
        except ValueError: # Evita bonificadores del tipo 50%
            #print "formato feo en " + atributo["Name"]
            pass
    return (
    (atributacos["Mana"]+atributacos["All Attributes"])*vector["Mana"] +
    (atributacos["Health"]+atributacos["All Attributes"])*vector["Health"] +
    (atributacos["Mana Regeneration"]+atributacos["All Attributes"])*vector["Mana Regeneration"] +
    (atributacos["Strength"]+atributacos["All Attributes"])*vector["Strength"] +
    (atributacos["Damage"]+atributacos["All Attributes"])*vector["Damage"] +
    (atributacos["Intelligence"]+atributacos["All Attributes"])*vector["Intelligence"] +
    (atributacos["Spell Resistance"]+atributacos["All Attributes"])*vector["Spell Resistance"] +
    (atributacos["Armor"]+atributacos["All Attributes"])*vector["Armor"] +
    (atributacos["Movement Speed"]+atributacos["All Attributes"])*vector["Movement Speed"] +
    (atributacos["Attack Speed"]+atributacos["All Attributes"])*vector["Attack Speed"]
    )
class Item(object):

    def __init__(self, id, name, cost, attribute, fitness, vector, weight=1):
        self.name = name
        self.cost = cost
        self.vector = vector
        self.attribute = attribute
        self.fitness = calculateFitness(attribute,vector)
        self.components = []
        self.weight = weight

    def __repr__(self):
        return "<{}>, <{}>, <{}>".format(self.name, self.attribute, self.fitness)
        # return "<%s | %d >" % (self.name, self.cost)

    def ratio(self):
        return float(self.cost) / (self.weight + self.cost)

    def addComponents(self, components):
        for i in range(0,len(components)):
            if components[i] is not None:                
                self.components.append(Item(components[i]["Id"], components[i]["ItemName"], components[i]["Cost"], components[i]["Attributes"], calculateFitness(components[i]["Attributes"], self.vector), self.vector))
                if components[i]["Components"] is None:
                    self.components[i].addComponents([])
                elif len(components[i]["Components"]) > 0:
                    self.components[i].addComponents(components[i]["Components"])

    def __eq__(self, item):
        return self.name == item.name
        

class Knapsack(object):

    def __init__(self, weight, gold, all_items, **kwargs):
        self.weight = self.initial_weight = weight
        self.gold = self.initial_gold = gold
        self.fitness = 0
        self.all_items = all_items
        self.initial_gold = gold
        self.items = []
        self.movement_counter = 0
        self.moves_made = []
        for key in kwargs.keys():
            setattr(self, key, kwargs[key])

    # heuristic_function is TabuSearch
    # neighborhood_function is the neighborhood_function attr in Tabu.py
    def optimize(self, initial_solution_function, heuristic_function, neighborhood_function):
        start = clock()
        initial_solution_function(self)
        self.initial_solution = deepcopy(self.items)
        self.initial_fitness = self.fitness
        heuristic_function(neighborhood_function, self)
        end = clock()
        print 'Best solution found with %d move(s).' % len(self.moves_made)
        print 'Initial solution was: %s' % self.initial_solution
        print 'Movements made: '
        for move in self.moves_made:
            print '-'*3 + " %s " % str(move)
        print 'Initial fitness: %d' % self.initial_fitness
        print 'Final fitness: %d' % self.fitness
        print 'Total improvement: %d' % (self.fitness - self.initial_fitness)
        print 'Weight left: %d' % self.weight
        print 'Initial Gold: %d' % self.initial_gold
        print 'Gold left: %d' % self.gold
        print 'Number of items in solution: %s' % len(self.items)
        print 'List of items:'
        for i in self.items:
            print "--- "+i.__repr__()
        time_spend = (end-start)*1000
        print 'Ran in %f milliseconds.' % (time_spend)
     
        try:
            readme = open('./tiempos.csv', 'a+')
            global num_attr
            readme.write("{};{};{};{}\n".format(self.initial_weight, self.initial_gold,num_attr,time_spend))
            readme.close()
        except:
            print "Error al escribir/abrir el fichero"

    def execute_movement(self, movement, silent=False):
        for item in movement.remove_items:
            if not item in self:
                return False
            self.remove_item(item)
        for item in movement.add_items:
            if not self.can_add_item(item):
                return False
            self.add_item(item)
        if not silent:
            self.movement_counter += 1
            self.moves_made.append(movement)
        return True

    def add_item(self, item):
        if self.can_add_item(item):
            self.items.append(item)
            self.weight -= item.weight
            self.gold -= item.cost
            self.fitness += item.fitness
            self.all_items.remove(item)
            return True
        return False

    def evaluate_swap(self, item, another_item):
        return self.can_swap(item, another_item)

    def remove_item(self, item):
        if item in self.items:
            self.weight += item.weight
            self.gold += item.cost
            self.fitness -= item.fitness
            self.items.remove(item)
            self.all_items.append(item)
            return True
        return False

# CUIDADO CON EL RETURN
    def can_swap(self, inside_item, another_item):
        if not inside_item in self or another_item in self:
            return False
        new_weight = self.weight + inside_item.weight
        new_gold = self.gold + inside_item.cost
        if (another_item.weight <= new_weight and another_item.cost <= new_gold):
            return self.fitness - inside_item.fitness + another_item.fitness
        return False

    def swap(self, item, another_item):
        if self.can_swap(item, another_item):
            self.remove_item(item)
            self.add_item(another_item)
            return True
        return False

    def __contains__(self, item):
        return any(map(lambda x: x == item, self.items))

    def can_add_item(self, item):
        if (self.weight >= item.weight and self.gold >= item.cost and not item in self.items):
            return True
        return False

    def __repr__(self):
        return "<Knapsack (%d) %s>" % (len(self.items), repr(self.items))

    def sorted_items(self, items, key=Item.ratio):
        return sorted(items, key=key, reverse=True)

    def solution_neighborhood(self, f):
        return f(self)


class Movement(object):

    def __init__(self, add_items=[], remove_items=[]):
        self.add_items = add_items
        self.remove_items = remove_items

# QUIZAS ME HE COLADO CON ESTA
    @property
    def movement_avaliation(self):
        remove_fitness = add_fitness = 0
        if not len(self.remove_items) == 0:
            remove_fitness = reduce(lambda x, y: x + y, [item.fitness for item in self.remove_items])
        if not len(self.add_items) == 0:
            add_fitness = reduce(lambda x, y: x + y, [item.fitness for item in self.add_items])
        return add_fitness - remove_fitness

    def reverse(self):
        return Movement(add_items=self.remove_items, remove_items=self.add_items)

    def __eq__(self, another_move):
        if not isinstance(another_move, Movement):
            return False
        return self.add_items == another_move.add_items and self.remove_items == another_move.remove_items

    def __repr__(self):
        # return "<Improve %d>" %  self.movement_avaliation
        return "<Remove %s | Add %s | Improve %d>" % (self.remove_items, self.add_items, self.movement_avaliation)