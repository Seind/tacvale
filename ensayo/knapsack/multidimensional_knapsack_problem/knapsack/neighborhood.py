from knapsack import Movement
from random import choice, shuffle
from copy import deepcopy

def add_and_or_remove_neighborhood(knapsack):
    neighborhood = []
    improving = []
    shuffle(knapsack.items)
    for item in knapsack.items:
        if knapsack.can_add_item(item):
            movement = Movement(add_items=[item,])
            neighborhood.append(movement)
        else:
            weight_to_lose = item.weight
            cost_to_lose = item.cost
            items = deepcopy(knapsack.items)
            to_remove = choice(items)
            items.remove(to_remove)
            while not knapsack.can_swap(to_remove, item):
                print to_remove
                print item
                print '-'*80
                if len(items) == 0:
                    break
                to_remove = choice(items)
                items.remove(to_remove)
            else:
                movement = Movement(add_items=[item,], remove_items=[to_remove,])
                neighborhood.append(movement)
            print neighborhood
    return neighborhood


def all_neighborhood(knapsack):
    neighborhood = []
    improving_solutions = []
    shuffle(knapsack.all_items)

    for item in knapsack.all_items:
        i = 0
        #actual_cost = knapsack.cost # ESTA VARIABLE NO SE USA?????
        shuffle(knapsack.items)

        to_remove = []
        cost_to_lose = item.cost
        weight_to_lose = item.weight
        while cost_to_lose > 0 and weight_to_lose > 0:
            solution_item = choice(knapsack.items)
            to_remove.append(solution_item)
            weight_to_lose -= solution_item.weight
            cost_to_lose -= solution_item.cost
            i += 1
            movement = Movement(add_items=[item,], remove_items=to_remove)
            neighborhood.append(movement)

        for solution_item in knapsack.items:
            if knapsack.can_add_item(item):
                movement = Movement(add_items=[item,])
                neighborhood.append(movement)
            elif knapsack.can_swap(solution_item, item):
                new_cost = knapsack.evaluate_swap(solution_item, item)
                movement = Movement(add_items=[item,], remove_items=[solution_item,])
                neighborhood.append(movement)
            else:
                continue
    return neighborhood

def first_improving_neighborhood(knapsack):
    neighborhood = []
    improving_solution = None
    for item in knapsack.sorted_items(knapsack.all_items):
        #actual_cost = knapsack.cost  ESTA VARIABLE NO SE USA?????
        for solution_item in knapsack.sorted_items(knapsack.items):
            if knapsack.can_swap(solution_item, item):
                new_cost = knapsack.evaluate_swap(solution_item, item)
                movement = Movement(add_items=[item,], remove_items=[solution_item,])
                if new_cost > knapsack.fitness: # Cuidado con esta, aunque creo fuerte que es asi
                    improving_solution = movement
                    neighborhood.append(movement)
                    return neighborhood
                neighborhood.append(movement)
            else:
                pass
    return neighborhood