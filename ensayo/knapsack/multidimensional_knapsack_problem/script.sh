### Pruebas cambiando oro
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=10000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=15000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=25000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=30000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=35000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=40000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=45000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=50000

### Pruebas con menos items
python2 main.py --file=25 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=35 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=50 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=65 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=80 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=100 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000

###### Pruebas cambiando tamano mochila
python2 main.py --file=128 --size=6 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=12 --attr=0,0,0,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=20 --attr=0,0,1,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=25 --attr=0,0,1,0,0,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=35 --attr=0,0,1,0,1,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=40 --attr=0,0,1,0,1,1,0,0,0,0,0,0 --gold=20000
python2 main.py --file=128 --size=45 --attr=0,0,1,0,1,1,0,0,0,0,0,1 --gold=20000
python2 main.py --file=128 --size=50 --attr=0,0,1,0,1,1,0,0,0,0,0,1 --gold=20000
python2 main.py --file=128 --size=55 --attr=0,0,1,0,1,1,0,0,0,0,0,1 --gold=20000
python2 main.py --file=128 --size=60 --attr=0,0,1,0,1,1,0,0,0,0,0,1 --gold=20000
python2 main.py --file=128 --size=65 --attr=0,0,1,0,1,1,0,0,0,0,0,1 --gold=20000
python2 main.py --file=128 --size=70 --attr=0,0,1,0,1,1,0,0,0,0,0,1 --gold=20000