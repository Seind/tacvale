# -*- coding: utf-8 -*-

import numpy as np, sys, time

def descomposicion(num):		
	factores = {}					#Coste = 1
	a = 2							#Coste = 1
	while (num>1):					#Coste Total = 1 + N*(1+M*(2+1)+2)? 
									#A medida que crece num. crece el n�mero de iteraciones del bucle...
		factores[a] = 0					#Coste = 1
		while (num % a == 0):			#Coste Total = 1+M*(2+1)
			num /= a						#Coste = 1
			factores[a] += 1				#Coste = 1
		#Limpieza de numeros sin usar
		if factores[a] == 0:			#Coste Total = 1 + 1
			del factores[a]					#Coste = 1
		a += 1								#Coste = 1
	return factores					#Coste = 1
#Coste Total de descomposicion(num) = 3 + N*(1+M*(3)+2) -> 3 + 3N + 3NM -> (NM + N + 1)*3

def getFactorescomunes(pepe, pepa):
	maxd = 1						#Coste = 1
	for key in pepe:				#Coste Total = P*2, P = n�mero de numeros primos encontrados que forman el n�mero
		if key in pepa:					#Coste Total = 2
			maxd *= key**pepe[key] if pepe[key]< pepa[key] else key**pepa[key]		#Coste = 1

	return maxd		#Coste = 1
#Coste Total de getFactorescomnes(pepe, pepa) = 1 + 2P

# P seguro que es < N*M ya que P se obtiene mediante N

start_time = time.time()		#Coste = 1
#---------------------------- 
num1 = sys.argv[1] 		#Se pasan ya como uint64		#Coste = 1
num2 = sys.argv[2] 		#Se pasan ya como uint64		#Coste = 1

longitudNumeros = len(str(num1)) + len(str(num2))		#Coste = 1

f = open("tiempos/1_1", "a")		#Coste = 1

#print(num1, num2)

des1 = descomposicion(num1)		#Coste = 1 + T(descomposicion)
des2 = descomposicion(num2)		#Coste = 1 + T(descomposicion)

#print(des1, des2)

res = getFactorescomunes(des1, des2)		#Coste = 1 + T(getFactorescomunes)
#----------------------------		#Coste = 
elapsed_time = time.time() - start_time		#Coste = 1

f.write(str(elapsed_time)+';'+str(longitudNumeros)+';'+str(res)+"\n")		#Coste = 1
f.close()		#Coste = 1

'''
	Coste definitivo (Obviando los imports):
		1+1+1+1+1+2+2T(descomposicion)+1+T(getFactoresComunes)+1+1+1
	   De aqui lo m�s costoso son las funciones
		2T(descomposicion)+T(getFactoresComunes)
	   El coste de descomposicion es muchisimo mayor que el de getFactores
	    T(descomposicion) = (NM + N + 1)*3.
	   Suponiendo que en el peor caso N y M son iguales --> N�

'''