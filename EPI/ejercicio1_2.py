# -*- coding: utf-8 -*-

import time, numpy as np

def euclides(dividendo, divisor):		
	if (dividendo % divisor == 0):		# Coste = 1 + 1
		return divisor					# Coste = 1
	else:
		return euclides(divisor, dividendo % divisor) 	# Coste 1 + 1 + 1 + 1 Coste(f')

start_time = time.time()		# Coste = 1
#----------------------------

num1 = sys.argv[1] 	#Se pasan ya como uint64			# Coste = 1
num2 = sys.argv[2] 	#Se pasan ya como uint64			# Coste = 1

longitudNumeros = len(str(num1)) + len(str(num2))			# Coste = (1 + 1 +1)*2 + 1 = 7

f = open("tiempos/1_2", "a")					# Coste = 1

res = euclides(num1, num2)			# Coste = 1 + Coste(f)
#----------------------------
elapsed_time = time.time() - start_time 	# Coste = 1 + 1

f.write(str(elapsed_time)+';'+str(longitudNumeros)+";"+str(res)+"\n")		# Coste = 1
f.close()		# Coste = 1

'''
Coste definitivo (Obviando los imports):
		1 + casoMasCostoso-> 4 + coste (f' con parámetros n y m con n>m)
	  Teniendo en cuenta que las llamadas a f no dependen linealmente
	  del tamaño de n y m, esto se asemejaría a log(n) donde no tengo ni
	  guarra de como explicar que es n ¿¿¿???
	  https://es.wikipedia.org/wiki/Algoritmo_de_Euclides  
'''