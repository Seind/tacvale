#-*- coding:utf-8 -*-
import sys, time

def calcular(num):                  #Coste Total = 17 + 5N
    #Con comprobación de número par
    if num == 1 or (num != 2 and int(str(num)[-1]) % 2 == 0): # Coste total = 1 +1 + 5 +1 + 1 + 2 + (1) = 12
        return -1

    num_prueba = 2                  # Coste = 1
    while num_prueba < num:         # Coste = 1 + N*Cuerpo -> Coste total = 1 + N(1+4)
        if num % num_prueba == 0:       # Coste = 2
            return -1                       # Coste = 1
        else:
            num_prueba += 1         # Coste = 2

    if num == num_prueba:           # Coste total = 2
        return 1
    else:
        return 0

f = open("tiempos/2_v1", "a")       #Coste = 1
start_time = time.time()

num = sys.argv[1]

res = calcular(num)

elapsed_time = time.time() - start_time

f.write(str(elapsed_time)+';'+str(len(str(num)))+';'+str(res)+"\n")
f.close()       
