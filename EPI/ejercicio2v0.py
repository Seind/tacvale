#-*- coding:utf-8 -*-
import sys, time

def calcular(num):				#Coste Total = 5 + 5N

	#Sin comprobación de si es par o no
	num_prueba = 2					# Coste = 1
	while num_prueba < num:			# Coste = N*Cuerpo + 1 -> Coste total = N(1+4) + 1
	    if num % num_prueba == 0:		# Coste = 2 -> Coste total = 4
	        return -1						# Coste = 1
	    else:
	        num_prueba += 1					# Coste = 2

	if num == num_prueba:			# Coste total = 2
	    return 1
	else:
	    return 0

f = open("tiempos/2_v0", "a")
start_time = time.time()

num = sys.argv[1]

res = calcular(num)

elapsed_time = time.time() - start_time

f.write(str(elapsed_time)+';'+str(len(str(num)))+';'+str(res)+"\n")
f.close()		
