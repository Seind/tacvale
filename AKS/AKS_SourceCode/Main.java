import java.math.BigInteger;
import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;

//https://www.mersenne.org/primes/
//https://primes.utm.edu/curios/index.php?start=12&stop=15

public class Main {

  public static BufferedReader lecturaLn(String path) {  
    try {
      Charset charset = Charset.forName("UTF-8");
      Path p = Paths.get(path);
      return Files.newBufferedReader(p, charset);
    } catch (IOException e) {
      System.out.println(e);
      return null;
    }
  }


  /**
   * @param args
   */
  public static void main(String[] args) {
    // TODO Auto-generated method stub    
    boolean cert = false;
    
    for(int i = 0; i < args.length; i++)
    {
      if(args[i].trim().equals("-c"))
        cert=true;
      if(args[i].trim().equals("-v"))
        Interface.verbose = true;
      if(args[i].trim().equals("-vs"))
      {
        Interface.verbose = true;
        MillerRabin.verbose = true;
        AKS.verbose = true;
        MillerRabinThread.verbose = true;
      }
    }
    
    try {
      BufferedReader datos = lecturaLn("numeros_primalidad");
      /*java.io.BufferedReader stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
      System.out.println("Type a new number:");
      String line = stdin.readLine();*/
      String line = "";
      while ((line = datos.readLine()) != null)
      {
        //Interface.verbose = false;
        AKS i = new AKS(new BigInteger(line));
        i.isPrimeTimed();
        // System.out.println((i.isPrime() ? "1" : "0"));
        //System.out.println((i.isPrime() ? "1 - PRIME" : "0 - NOT PRIME"));
        
        //System.out.println("Type a new number:");
        //numerico = stdin.readLine();
      }
    }
    catch(Exception e)
    {
      System.out.println(e.getMessage());
    }
    
    /*
    BigInteger totest = new BigInteger("1519380").pow(32768).add(BigInteger.ONE);
    MillerRabin mr = new MillerRabin(totest,10000); 
    if(mr.isPrime())
      System.out.println(totest.toString() + " is prime");
    else
      System.out.println(totest.toString() + " is NOT prime");
    */
    

  }

}

