import java.math.BigInteger;
import java.lang.Thread;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
/**
 * @author Vincent
 *
 */
public class AKS extends Thread
{

	static boolean verbose = false;
	
	BigInteger n;
	boolean n_isprime;
	BigInteger factor;
	double timeelapsed;
	BigInteger contador =  BigInteger.valueOf(0);

	/***
	 * Constructor--just save the number
	 * @param n
	 */
	public AKS(BigInteger n)
	{
		this.n = n;
	}
	
	/***
	 * Run AKS.isprime as a thread
	 */
	public void run()
	{
	  this.isPrime();
	}

	public int write(String path, String texto) {
		try {
			Path p = Paths.get(path);
			Charset charset = Charset.forName("UTF-8");
			//Como no queremos que lo resetee (TRUNCATE) hay que ponerle APPEND
			BufferedWriter writer = Files.newBufferedWriter(p, charset,
			                      StandardOpenOption.APPEND);
			writer.write(texto, 0, texto.length());
			//Importante cerrar el escritor, ya que si no, no escribe
			writer.close();
			return 0;
		} catch (IOException e) {
			System.out.println(e);
			return 1;
		}
	}
	
	/***
	 * Run the AKS primality test and time it
	 * 
	 * @return true if n is prime
	 */
	public boolean isPrimeTimed()
	{
		double start = System.currentTimeMillis();
		boolean rtn = isPrime();
		timeelapsed = System.currentTimeMillis() - start;
		write("tiempos/hito3.txt", ""+timeelapsed+";"+contador+";"+n+"\n");
		return rtn;
	}
	
	/***
	 * Run the AKS primality test
	 * 
	 * @return true if n is prime
	 */
	public boolean isPrime() 
	{
		// TODO: Do this in linear time http://www.ams.org/journals/mcom/1998-67-223/S0025-5718-98-00952-1/S0025-5718-98-00952-1.pdf
		// If ( n = a^b for a in natural numbers and b > 1), output COMPOSITE
		/*
		BigInteger base = BigInteger.valueOf(2);
		BigInteger aSquared;
		do
		{
			BigInteger result;

			int power = Math.max((int) (log()/log(base) - 2),1); // 1
			int comparison;	 // 1

			//LOQUESEA
			do
			{
				power++;	// 1
				result = base.pow(power); //N  Eleva la base a la potencia.
				comparison = n.compareTo(result); // -1 si es menor, 0 igual, 1 mayor
			}
			while( comparison > 0 && power < Integer.MAX_VALUE );

			//Constante y con primos nunca se da
			if( comparison == 0 )
			{
				if (verbose) {
					System.out.println(n + " is a perfect power of " + base);
				}
				factor = base;
				n_isprime = false;
				return n_isprime;
			}
			
			//Constante y no se da porque está a false
			if (verbose) {
				System.out.println(n + " is not a perfect power of " + base);
			}

			base = base.add(BigInteger.ONE); // 1
			aSquared = base.pow(2);
		}
		while (aSquared.compareTo(this.n) <= 0);

		if (verbose){
			System.out.println(n + " is not a perfect power of any integer less than its square root");
		}
		
		////////////////////////////FIN HITO 1 ////////////////////////// */

		////////////////// COMIENZO HITO 2
		// Find the smallest r such that o_r(n) > log^2 n
		// o_r(n) is the multiplicative order of n modulo r
		// the multiplicative order of n modulo r is the 
		// smallest positive integer k with	n^k = 1 (mod r).
		double log = this.log();
		double logSquared = log*log;
		BigInteger k = BigInteger.ONE;
		BigInteger r = BigInteger.ONE;
		//** 4
		do
		{
			r = r.add(BigInteger.ONE); // 1
			if (verbose) System.out.println("trying r = " + r);
			k = multiplicativeOrder(r); //log5(n)
		}
		while( k.doubleValue() < logSquared ); //Mientras K sea < que el log(n)²
		//**Coste del do-while: (1+log5(n)) * (n-1)
		// siendo brutos, el peor caso es que se tengan que probar todos los números excepto n,
		// ya que si n=r no existe n^k mod(r) != 0

		if (verbose) System.out.println("r is " + r);

		
		// If 1 < gcd(a,n) < n for some a <= r, output COMPOSITE
		for( BigInteger i = BigInteger.valueOf(2); i.compareTo(r) <= 0; i = i.add(BigInteger.ONE) )
		{
			BigInteger gcd = n.gcd(i);
			if (verbose) System.out.println("gcd(" + n + "," + i + ") = " + gcd);
			//Mientras result sea DSITINTO de 1 y r sea MAYOR que k
			if ( gcd.compareTo(BigInteger.ONE) > 0 && gcd.compareTo(n) < 0 )
			{
				factor = i;
				n_isprime = false;
				return false;
			}
		}
		//**El bucle for se repite N-1 veces, ya que va desde 2 hasta  = a r
		
		
		// If n <= r, output PRIME
		if( n.compareTo(r) <= 0 )
		{
			n_isprime = true;
			return true; //**Coste 2 el if
		}

		//Complejidad T(n) = 4 + (1+log5(n)*repeticiones) + (N-1) + 2
		//Complejidad O(n) = N + (1+log5(n)*repeticiones)
							 //De aqui la mayor complejidad una vez sepamos las repeticiones
		///////////////// FIN HITO 2

		///////////////// COMIENZO HITO 3
		// For i = 1 to sqrt(totient)log(n) do
		// if (X+i)^n <> X^n + i (mod X^r - 1,n), output composite;

		// sqrt(totient)log(n)
		int limit = (int) (Math.sqrt(totient(r).doubleValue()) * this.log());
		// X^r - 1
		Poly modPoly = new Poly(BigInteger.ONE, r.intValue()).minus(new Poly(BigInteger.ONE,0));
		// X^n (mod X^r - 1, n)
		Poly partialOutcome = new Poly(BigInteger.ONE, 1).modPow(n, modPoly, n);
		contador = contador.add(BigInteger.valueOf(3)); //** Supongamos que son 3 pasos bases las cosas estas
		//** Se repetirá desde 1 hasta sqrt(totient)log(n). Dado que empieza en 1, se resta 1 al total:
		//** 2*sqrt(r)*log2(n) --> De la bibliografía (fotos) sacamos que r como máximo vale log2(n)^5
		//** 2*sqrt(log2(n)^5)*log2(n) --> log2(n)^4??????
		contador = contador.add(BigInteger.valueOf(2)); //** Inicialización mas comparación
		for( int i = 1; i <= limit; i++ )
		{
			Poly polyI = new Poly(BigInteger.valueOf(i),0);
			// X^n + i (mod X^r - 1, n)
			Poly outcome = partialOutcome.plus(polyI);
			Poly p = new Poly(BigInteger.ONE,1).plus(polyI).modPow(n, modPoly, n);
			contador = contador.add(BigInteger.valueOf(3)); //Supongamos que son 3 pasos bases
			if( !outcome.equals(p) )
			{
				if (verbose) System.out.println( "(x+" + i + ")^" + n + " mod (x^" + r + " - 1, " + n + ") = " + outcome);
				if (verbose) System.out.println( "x^" + n + " + " + i + " mod (x^" + r + " - 1, " + n + ") = " + p);
				// if (verbose) System.out.println("(x+i)^" + n + " = x^" + n + " + " + i + " (mod x^" + r + " - 1, " + n + ") failed");
				factor = BigInteger.valueOf(i);
				n_isprime = false;
				contador = contador.add(BigInteger.valueOf(3)); //2 pasos + return
				return n_isprime;
			}
			else
				if (verbose) System.out.println("(x+" + i + ")^" + n + " = x^" + n + " + " + i + " mod (x^" + r + " - 1, " + n + ") true");

			contador = contador.add(BigInteger.valueOf(2)); //i++ y evaluar condición
		}
		
		n_isprime = true;
		contador = contador.add(BigInteger.valueOf(2)); //1 pasos + return
	    return n_isprime;
	    /////////////////////// FIN HITO 3
	}
	
	/***
	 * Calculate the totient of a BigInteger r
	 * Based on this algorithm:
	 * 
	 * http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=primeNumbers
	 * 
	 * @param r BigInteger to calculate the totient of
	 * @return phi(r)--number of integers less than r that are coprime
	 */
    BigInteger totient(BigInteger n) 
    { 
    	BigInteger result = n; 
      
      	//** Se repite desde 2 y mientras r sea mayor que i*i -> ceil(log2(r)) veces
    	for( BigInteger i = BigInteger.valueOf(2); n.compareTo(i.multiply(i)) > 0; i = i.add(BigInteger.ONE) ) 
    	{ 
    		if (n.mod(i).compareTo(BigInteger.ZERO) == 0) 
    			result = result.subtract(result.divide(i));
    		
    		//Siendo primo nunca se entra.
    		while (n.mod(i).compareTo(BigInteger.ZERO) == 0)
    			n = n.divide(i);
    	}
    	
    	if (n.compareTo(BigInteger.ONE) > 0) 
    		result = result.subtract(result.divide(n));
    	
    	return result;
    	
    } 

	/***
	 * Calculate the multiplicative order of n modulo r
	 * This is defined as the smallest positive integer k 
	 * for which n^k = 1 (mod r).
	 * 
	 * @param r modulus for mutliplicative order
	 * @return multiplicative order or -1 if none exists
	 */
	//Complejidad de log5(n)
	BigInteger multiplicativeOrder(BigInteger r)
	{
		// TODO Consider implementing an alternative algorithm http://rosettacode.org/wiki/Multiplicative_order
		BigInteger k = BigInteger.ZERO;
		BigInteger result;
		
		do
		{
			k = k.add(BigInteger.ONE);
			result = this.n.modPow(k,r);
		}
		//Mientras result sea DSITINTO de 1 y r sea MAYOR que k
		while( result.compareTo(BigInteger.ONE) != 0 && r.compareTo(k) > 0);
		
		if (r.compareTo(k) <= 0)
			return BigInteger.ONE.negate();
		else
		{
			if (verbose) System.out.println(n + "^" + k + " mod " + r + " = " + result);
			return k;
		}
	}
	

	// Save log n here
	double logSave = -1;

	/***
	 * 
	 * @return log base 2 of n
	 */
	double log()
	{
		if ( logSave != -1 )
			return logSave;
		
		// from http://world.std.com/~reinhold/BigNumCalcSource/BigNumCalc.java
		BigInteger b;
		
	    int temp = n.bitLength() - 1000;
	    if (temp > 0) 
	    {
	    	b=n.shiftRight(temp); 
	        logSave = (Math.log(b.doubleValue()) + temp)*Math.log(2);
	    }
	    else 
	    	logSave = (Math.log(n.doubleValue()))*Math.log(2);

	    return logSave;
	}

	
	/**
	 * log base 2 method that takes a parameter
	 * @param x
	 * @return
	 */
	double log(BigInteger x)
	{
		// from http://world.std.com/~reinhold/BigNumCalcSource/BigNumCalc.java
		BigInteger b;
		
	    int temp = x.bitLength() - 1000;
	    if (temp > 0) 
	    {
	    	b=x.shiftRight(temp); 
	        return (Math.log(b.doubleValue()) + temp)*Math.log(2);
	    }
	    else 
	    	return (Math.log(x.doubleValue())*Math.log(2));
	}
	
	public BigInteger getFactor()
	{
		return factor;
	}

  public double GetElapsedTime() {
    return timeelapsed;
  }
	
}
